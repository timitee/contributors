![](https://elioway.gitlab.io/elioway/contributors/elio-contributors-logo.png)

> Keeping track of contributors, **the elioWay**

# contributors

This repo is used to keep track of contributors of the **elioWay**, **the elioWay**.

Interested people who want to help, can get in touch by adding themselves to the **list** of contributors and making a pull request.

## Contributing

1. Fork this repo [contributors](https://elioway.gitlab.io/contributors)
2. Clone it locally. `git clone git@gitlab.com:myForkOf/contributors`
3. Enter it. `cd contributors`
4. `npm i -g @elioway/bones @elioway/thing`
5. Use the **bones** CLI to add yourself to the **list**
6. Submit a merge request.

## License

[MIT](license)

![](https://elioway.gitlab.io/elioway/contributors/apple-touch-icon.png)
