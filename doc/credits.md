# contributors Credits

## Core Thanks!

- [elioWay](https://elioway.gitlab.io)
- [Yeoman](http://yeoman.io/)

## Artwork

- [People-puzzle-frame-silhouette](https://publicdomainvectors.org/en/free-clipart/People-puzzle-frame-silhouette/81939.html)
- [Two_women_operating_ENIAC](https://commons.wikimedia.org/wiki/File:1960%27s_Mainframe_Computer_at_Buss_Farm_-_geograph.org.uk_-_1957378.jpg)
