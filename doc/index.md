# contributors

<figure>
  <img src="star.png" alt="">
</figure>

> Contributors, **the elioWay**

![experimental](/eliosin/icon/devops/experimental/favicon.ico "experimental")

## WTF

If you want to contribute, the best way to is to add yourself to the `contributors.json` of this repo and request a merge.

See [Quickstart for contributors](quickstart.html)
