# Prerequisites for contributors

- A public profile account somewhere. GIT repo is perfect. Facebook, LinkedIn or any other way we can message you is fine.
- Basic knowledge of GIT.
- Basic knowledge using a Command Line.
- Basic knowledge of JSON.

  - [wtf is a Jason Oject?](/wtf-json.html)

- Installed NodeJs and npm runtimes.

  - <https://nodejs.org/en/download/>
