# Quickstart for contributors

- [contributors Prerequisites](/elioway/contributors/prerequisites.html)
- [Installing contributors](/elioway/contributors/installing.html)

## Nutshell

Add yourself to the **list** of **contributors** in this repo. Tell us where your primary GIT account is or some other public facing social account you use, like Twitter, Facebook, StackOverflow, etc. A sentence on how you would like to help, or need help.

**IMPORTANT: THIS LIST IS PUBLIC. DO NOT ADD ANY CONTACT DETAILS**

Only add a url and a description of your interest.

1. Fork this repo [contributors](https://elioway.gitlab.io/contributors)

- Project name: `contributors`
- Project URL: `yourOwnRepo/contributors`
- Visibility level: `Public`

2. Clone it locally. `git clone git@gitlab.com:yourOwnRepo/contributors`

3. Enter it. `cd contributors`

4. Install our CLI tools. `npm i -g @elioway/bones @elioway/thing`

5. View the existing list using our CLI tools.

```
bones readT contributors
bones listT contributors --mainEntityOfPage=Person
```

6. Use the **bones** CLI to add yourself to the **list**. e.g. This is how I - "TimBushell" of "<https://gitlab.com/timitee>" added myself:

```
bones takeonT contributors TimBushell --mainEntityOfPage=Person

# No need for the `Person` part. Don't fill these fields out.
bones updateT TimBushell --Person=""

# If not a git repo, somewhere public where we can find you.
bones updateT TimBushell --url="https://gitlab.com/timitee"

# Briefly - how you would like to help.
bones updateT TimBushell --description="I would like to be a leader in the elioWay"

bones listT contributors --mainEntityOfPage=Person
```

7. Commit and push your changes to your own repo. This is a shared place to communicate with other contributors. Feel free to submit other notes or observations.

8. Submit a merge request. e.g.:

<https://gitlab.com/yourOwnRepo/contributors/-/merge_requests>

9. Keep track of the list.

```
git remote add upstream git@gitlab.com:elioway/contributors.git
git pull upstream main
```

10. Once we've added you to the elioWay contributors you will be able to push:

```
git push upstream main
```

## Handling SHA for Multiple Git Accounts

We use gitlab. You don't need a gitlab account but it will help. Or perhaps you already have a gitlab account, but would like to set up a different account, using a different SHA key... this is how you can maintain different keys for more than one GIT account.

Edit or create `~/.ssh/config`

```
Host gitlab.com
  HostName gitlab.com
  User git
  IdentityFile ~/.ssh/main

Host gitlab.ACCT1
  HostName gitlab.com
  User git
  IdentityFile ~/.ssh/key1

Host gitlab.ACCT2
  HostName gitlab.com
  User git
  IdentityFile ~/.ssh/key2

Host github.ACCT3
  HostName github.com
  User git
  IdentityFile ~/.ssh/key3
```

then make sure you set the right remote URL to each of your repo:

```
git remote set-url origin git@gitlab.ACCT1:myACCT1/myACCT1repo.git
```
